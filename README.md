# FiveM Vehicles

## Bereitstellen

Das Gitlab Repo muss unterhalb `resources` liegen. Wo ist eigentlich egal. 

```
git clone https://gitlab.com/markpumpe/fivem-vehicles [vehicles]
```

Danach gibt es einen Ordner mit Namen `[vehicles]`, in dem liegen die Fahrzeuge.

## Vorarbeiten

Damit man ein Fahrzeug auch mit `start fahrzeug` und `car fahrzeug` aktivieren kann, braucht man ein Script.
Das gibt es schon fertig unter https://docs.fivem.net/docs/scripting-manual/introduction/creating-your-first-script/#implementing-a-car-spawner

## Fahrzeuge

| Download Link | Im FiveM Format | Test | Spawnname |   |
|---|---|---|---|---|
| https://de.gta5-mods.com/vehicles/2017-hennessey-velociraptor-6x6-add-on-fivem-tuning | ja | erfolgreich  |   |   |
| amggtr | Nein | misslungen | amggtr |   |
| wraith | Nein | erfolgreich  | wraith |   |
| https://www.gta5-mods.com/vehicles/2017-nissan-gtr-r35-add-on-fivem-template-lods | Ja |  | 17r35 | |
| https://www.gta5-mods.com/vehicles/1970-ford-maverick-v8-add-on | Nein |  | | |
| https://www.gta5-mods.com/vehicles/2013-audi-rs4-avant | Nein | ? | 13rs4 |  |
| ? | Nein | ? | hemis | |
| https://www.gta5-mods.com/vehicles/tesla-roadster-2020-add-on-replace-auto-spoiler | Nein | ? | tr22 | |
| https://www.gta5-mods.com/vehicles/2019-mercedes-benz-maybach-rolf-hart-mr500-e5fae15e-4d37-4318-99c0-e534cd47f7ea | Nein | ? | oycmr500 | |
https://www.gta5-mods.com/vehicles/bmw-m8-competition-mansaug : mansm8
https://www.gta5-mods.com/vehicles/unmarked-caracara-sas994/download/113502 :polcaracara
https://de.gta5-mods.com/vehicles/bmw-m8-coupe-2020-add-on/download/77786 : m82020
https://de.gta5-mods.com/vehicles/brabus-g-class-g800/download/114023 :W463A1

https://www.gta5-mods.com/vehicles/2020-corvette-c8-stingray-add-on-oiv-tuning-template :stingray

https://www.gta5-mods.com/vehicles/bugatti-centodieci-2020-add-on-1-0/download/79794 :bugattticentodieci


https://www.gta5-mods.com/vehicles/bugatti-divo-2019 : divo

https://www.gta5-mods.com/vehicles/2020-bugatti-bolide-addon-sp-fivem/download/105750 : bolide

https://www.gta5-mods.com/vehicles/bugatti-chiron-mansory-centuria-replace/download/74568 :centuria

https://www.gta5-mods.com/vehicles/audi-rs7-abt-2021-add-on-fivem/download/113088 :rs7


https://www.gta5-mods.com/vehicles/1968-plymouth-formula-s-barracuda-hemi-s-add-on-9778e5db-d0aa-4ece-a130-0da0a16085db



https://www.gta5-mods.com/vehicles/volkswagen-golf-gti-mk7-2015-add-on-replace-digital-dials-dirt-map

https://www.gta5-mods.com/vehicles/audi-sq7-2016-add-on

https://de.gta5-mods.com/vehicles/2019-porsche-911-gt3-rs-add-on

Ausstehend: 

https://www.gta5-mods.com/vehicles/2016-volkswagen-passat-highline-stanced-b8-saleen

https://www.gta5-mods.com/vehicles/volkswagen-passat-variant-r-line

https://www.gta5-mods.com/vehicles/honda-civic-type-r-2017-add-on-replace

https://www.gta5-mods.com/vehicles/opel-corsa-d-opc-add-on

https://www.gta5-mods.com/vehicles/toyota-supra-add-on-stock-more-tuning

https://de.gta5-mods.com/vehicles/mazda-rx7-fd3s-add-on-tunable

https://de.gta5-mods.com/vehicles/mazda-rx8-spirit-r-2012

https://de.gta5-mods.com/vehicles/mercedes-benz-s500-w222

https://de.gta5-mods.com/vehicles/2009-maybach-62-s

https://www.gta5-mods.com/vehicles/toyota-hylux-revo-cap-2014-add-on

https://www.gta5-mods.com/vehicles/toyota-land-cruiser-200-2016



## Probleme

### Zu großes Modell

Äußert sich mit dieser Meldung:

```
Asset wraith/wraith.ytd uses 59.2 MiB of physical memory. Oversized assets can and WILL lead to streaming issues (models not loading/rendering, commonly known as 'texture loss', 'city bug' or 'streaming isn't great').
```

https://forum.cfx.re/t/problem-with-cars-and-skins/1654290



## Tools

https://alternativeto.net/software/openiv/

