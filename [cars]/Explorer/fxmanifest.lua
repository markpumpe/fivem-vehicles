fx_version 'cerulean'
game 'gta5'

description 'BMW M8 Competition'

files {
    'data/carcols.meta',
	'data/carvariations.meta',
	'data/vehicles.meta'
}

data_file 'VEHICLE_METADATA_FILE' 'data/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/carvariations.meta'

