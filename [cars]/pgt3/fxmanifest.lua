fx_version 'cerulean'
game 'gta5'

description '2019 Porsche 911 GT3 RS'

files {
	'data/carvariations.meta',
	'data/handling.meta',
	'data/vehicles.meta'
}

data_file 'HANDLING_FILE' 'data/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/carvariations.meta'