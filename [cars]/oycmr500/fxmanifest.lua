fx_version 'cerulean'
game 'gta5'

description 'Mercedes Benz Maybach 500'

files {
	'data/carvariations.meta',
	'data/handling.meta',
	'data/vehicles.meta'
}

data_file 'HANDLING_FILE' 'data/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/carvariations.meta'
